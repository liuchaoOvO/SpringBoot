#!/usr/bin/env bash

#服务相关配置
API_NAME=springboot-demo
ROOT_PATH=/home/admin/application/root/workspace/projectsource/springboot-demo/target



JAR_NAME=$ROOT_PATH/$API_NAME\.jar
#PID  代表是PID文件
PID=$ROOT_PATH/logs/$API_NAME\.pid
#log
LOG=$ROOT_PATH/logs/$API_NAME\.log


#使用说明，用来提示输入参数
usage() {
    echo "Usage: sh 执行脚本.sh [start|stop|restart|status|estop]"
    exit 1
}

#检查程序是否在运行
is_exist(){
  pid=`ps -ef|grep $JAR_NAME|grep -v grep|awk '{print $2}' `
  #如果不存在返回1，存在返回0
  if [ -z "${pid}" ]; then
   return 1
  else
    return 0
  fi
}

#启动方法，--spring.profiles.active=production设置启动production环境
start(){
  is_exist
  if [ $? -eq "0" ]; then
    echo ">>> ${JAR_NAME} is already running PID=${pid} <<<"
  else
   mkdir -p $ROOT_PATH/logs
   touch $ROOT_PATH/logs/{springboot-demo.log,springboot-demo.pid}
	 nohup java  -Dspring.profiles.active=prod -jar $JAR_NAME  --spring.profiles.active=prod > $LOG 2>&1 &
    echo $! > $PID
    echo ">>> start $JAR_NAME successed PID=$! <<<"
   fi
  }

#停止方法
stop(){
  #is_exist
  pidf=$(cat $PID)
  #echo "$pidf"
  echo ">>> api PID = $pidf begin kill $pidf <<<"
  kill $pidf
  rm -rf $PID
  sleep 2
  is_exist
  if [ $? -eq "0" ]; then
    echo ">>> api 2 PID = $pid begin kill -9 $pid  <<<"
    kill -9  $pid
    sleep 2
    echo ">>> $JAR_NAME process stopped <<<"
  else
    echo ">>> ${JAR_NAME} is not running <<<"
  fi
}

#输出运行状态
status(){
  is_exist
  if [ $? -eq "0" ]; then
    echo ">>> ${JAR_NAME} is running PID is ${pid} <<<"
  else
    echo ">>> ${JAR_NAME} is not running <<<"
  fi
}

#重启
restart(){
  stop
  start
}

#在Eureka主动离线并停止
estop(){
  echo ">>> unregistry Eureka = $EUREKA_SERVICE <<<"
  echo ">>> unregistry App = $EUREKA_APP_NAME <<<"
  echo ">>> unregistry Instance = $EUREKA_INSTANCE <<<"
  curl -v -X DELETE $EUREKA_SERVICE/apps/$EUREKA_APP_NAME/$EUREKA_INSTANCE
  stop
}

#根据输入参数，选择执行对应方法，不输入则执行使用说明
case "$1" in
  "start")
    start
    ;;
  "stop")
    stop
    ;;
  "status")
    status
    ;;
  "restart")
    restart
    ;;
  "estop")
    estop
    ;;
  *)
    usage
    ;;
esac
exit 0