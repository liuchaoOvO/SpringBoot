package com.lc.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api")
public class TestDemoController {

    @GetMapping("/testDemo")
    public String testDemo() {
        log.info("come into testDemo Controller");
        return "testDemo" + String.valueOf(System.currentTimeMillis());
    }

}
